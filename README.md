# This is a work in progress. It is not complete yet.

Role Name
=========

Ansible role that uses virt-clone to clone a guest. This role makes a couple of assumptions. The first is that 
you have created a template beforehand to clone the guests from. The second is that you are either using DHCP
reservations via MAC address or that you have some other way to get addresses of the systems that have been cloned.

Requirements
------------

QEMU/KVM

Role Variables
--------------

`template`: Name of the VM template to clone

`disk`: Name to use for the cloned disk

`mac`: MAC address to use

`hypervisor`: Remote host to clone the guest on.

`maxsize`: Max memory amount

`memsize`: Memory size for the guest

Dependencies
------------

QEMU/KVM

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

playbook.yml:
```yaml
    - hosts: kvm_servers
      roles:
         - kvm-clone
```

group_vars/kvm_servers:
```yaml
vm_location: "/data/VMs"

guests:
  guest1:
    hypervisor: kvm1
	template: centos7.5
	disk: guest1.qcow2
    memsize: 1
    maxsize: 1
	
```

License
-------

BSD

Author Information
------------------

John Hooks
